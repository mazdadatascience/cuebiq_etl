import datetime
import gzip
import os
import subprocess
from io import StringIO
from pathlib import Path

import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

## create sql connection
os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

## sync daily bucket
sync_cmd = "aws s3 sync s3://cuebiq-dataset-nv/1/8afaf189-dfe6-4686-a19d-176c74fe4f7c/290/ daily/ --profile cuebiqdaily"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stdout=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break

file_sources = pd.read_sql(
    "SELECT DISTINCT FILE_SOURCE FROM WP_CUEBIQ_DAILY_VISITS", engine
)

## read raw data and write to table

for f in Path("daily").iterdir():
    raw_feed = ""
    folder = f.name.lower()
    if folder not in list(file_sources["file_source"]):
        for raw_visits in f.iterdir():
            with gzip.open(raw_visits, "rb") as raw:
                raw_feed += raw.read().decode("utf8")
        cb_raw_df = pd.read_csv(
            StringIO(raw_feed),
            sep="\t",
            header=None,
            names=["cal_date", "cb_id", "place_id", "dlr_cd", "logfile"],
            dtype=str,
        )

        cb_raw_df["cal_date"] = pd.to_datetime(cb_raw_df["cal_date"], utc=True).dt.date
        cb_raw_df["file_source"] = folder
        cb_grouped = (
            cb_raw_df.groupby(
                ["cal_date", "cb_id", "place_id", "dlr_cd", "logfile", "file_source"]
            )
            .size()
            .reset_index(name="raw_visits")
        )
        cb_grouped.to_sql(
            "wp_cuebiq_daily_visits",
            engine,
            if_exists="append",
            index=False,
            chunksize=100000,
            dtype={
                "cal_date": DateTime(),
                "cb_id": String(100),
                "place_id": String(36),
                "dlr_cd": String(100),
                "raw_visits": Float(),
                "logfile": String(100),
                "file_source": String(100),
            },
        )
        print(f"Completed folder: {folder}")